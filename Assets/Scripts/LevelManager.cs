using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    
    #region Singleton

    private static LevelManager m_instance;

    public static LevelManager Instance => m_instance;

    private void Awake()
    {
        if (m_instance != null && m_instance != this)
            Destroy(gameObject);

        m_instance = this;
    }

    #endregion

    #region Private Fields
    
    private float m_roadLength;
    private float m_roadSpeed;
    private float m_distanceLeft;
    private float m_nextCheckpoint;
    private bool m_endRoad = false;
    private bool m_carUIActive;

    private List<RoadType> m_nextRoads = new List<RoadType>();
    
    #endregion
        
    #region Serialized Fields

    [SerializeField] private Road[] m_roadChunks;
    [SerializeField] private GameObject[] m_graphicModules;
    [SerializeField] private float m_roadWidth = 6f;
    [SerializeField] private float m_roadChunkLength = 15f; // length of roads

    #endregion
    
    #region Getters

    public int CurrentTutoStep;
    public float DistanceLeft => m_distanceLeft;
    public int DistanceMade => (int)GameManager.Data.LevelDistance - (int)m_distanceLeft;
    public GameObject[] GraphicModules => m_graphicModules;
    public float RoadWidth => m_roadWidth;
    public float RoadChunkLength => m_roadChunkLength; // length of roads
    public float RoadSpeed { get => m_roadSpeed; set { m_roadSpeed = value; } }
    public float CarSpeed => RoadSpeed * 3.6f; // m/s -> km/h
    public bool EndRoad => m_endRoad;
    
    #endregion

    void Start()
    {
        m_roadSpeed = GameManager.Data.StartYSpeed;
        m_roadLength = m_roadChunkLength * m_roadChunks.Length;
        m_distanceLeft = GameManager.Data.LevelDistance;
        m_nextCheckpoint = GameManager.Data.DistanceBetweenCheckpoints;
        ResetRoads();
    }

    public void Initialize()
    {
        m_distanceLeft = GameManager.Data.LevelDistance;
        m_nextCheckpoint = GameManager.Data.DistanceBetweenCheckpoints;
        m_nextRoads.Clear();

        if (!GameManager.Instance.TutoCompleted)
        {
            CurrentTutoStep = 0;
            for (int i = 0; i < Control.Controls.Count; i++)
            {
                m_nextRoads.Add(RoadType.EMPTY);
                m_nextRoads.Add(RoadType.OBSTACLES);
                m_nextRoads.Add(RoadType.EMPTY);
                m_nextRoads.Add(RoadType.CHECKPOINT);
                m_nextRoads.Add(RoadType.EMPTY);
            }
#if UNITY_ANDROID && !UNITY_EDITOR
            m_nextRoads.Add(RoadType.EMPTY);
            m_nextRoads.Add(RoadType.OBSTACLES);
            m_nextRoads.Add(RoadType.EMPTY);
            m_nextRoads.Add(RoadType.CHECKPOINT);
            m_nextRoads.Add(RoadType.EMPTY);
#endif
        }
    }

    void Update()
    {
        if (GameManager.Instance.IsOver)
            return;

        if (GameManager.Instance.IsStarted)
        {
            if (GameManager.Instance.TutoCompleted)
            {
                m_roadSpeed += GameManager.Data.AccelerationCurve.Evaluate(m_roadSpeed / GameManager.Data.MaxSpeed) * Time.deltaTime;
                m_distanceLeft -= m_roadSpeed * Time.deltaTime;
                m_nextCheckpoint -= m_roadSpeed * Time.deltaTime;
            }
            else if (m_roadSpeed < GameManager.Data.StartYSpeed)
            {
                m_roadSpeed += GameManager.Data.AccelerationCurve.Evaluate(m_roadSpeed / GameManager.Data.MaxSpeed) * Time.deltaTime;
                StartCoroutine(CarController.Instance.DisplayUI(true));
                m_carUIActive = true;
            }
            else
            {
                if (m_carUIActive)
                {
                    StartCoroutine(CarController.Instance.DisplayUI(false));
                    m_carUIActive = false;
                }
            }
        }

        foreach (Road road in m_roadChunks)
        {
            Vector3 newRoadPos = road.transform.localPosition;
            newRoadPos.z -= m_roadSpeed * Time.deltaTime;
            if (newRoadPos.z < -m_roadChunkLength)
            {
                newRoadPos.z += m_roadLength;
                
                if (!GameManager.Instance.IsStarted)
                    m_nextRoads.Insert(0, RoadType.EMPTY);
                else if (m_distanceLeft <= 0)
                {
                    m_nextRoads.Add(RoadType.END_WALL);
                    m_nextRoads.Add(RoadType.EMPTY);
                    m_nextRoads.Add(RoadType.EMPTY);
                    m_nextRoads.Add(RoadType.EMPTY);
                    m_nextRoads.Add(RoadType.EMPTY);
                    m_nextRoads.Add(RoadType.EMPTY);
                }
                else if (m_nextCheckpoint <= 0)
                {
                    m_nextRoads.Add(RoadType.EMPTY);
                    m_nextRoads.Add(RoadType.CHECKPOINT);
                    m_nextRoads.Add(RoadType.EMPTY);
                    m_nextCheckpoint = GameManager.Data.DistanceBetweenCheckpoints;
                }
                else
                    m_nextRoads.Add(RoadType.OBSTACLES);


                RoadType roadType = RoadType.EMPTY;
                
                if (m_nextRoads.Count > 0)
                {
                    roadType = m_nextRoads[0];
                    m_nextRoads.RemoveAt(0);
                }
                
                road.GenerateRoads(roadType);
            }

            road.transform.localPosition = newRoadPos;
        }
    }

    private void ResetRoads()
    {
        foreach (Road road in m_roadChunks)
        {
            road.GenerateRoads(RoadType.EMPTY);
        }
    }

    public void ClearRoad()
    {
        foreach (Road road in m_roadChunks)
        {
            for (int index = 0; index < road.ObstacleParent.childCount; index ++)
            {
                Destroy(road.ObstacleParent.GetChild(index).gameObject);
            }
        }
    }

    public IEnumerator LerpSpeed(float newSpeed)
    {
        float startValue = m_roadSpeed;
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime * GameManager.Data.SpeedChangeLerpSpeed;
            m_roadSpeed = Mathf.SmoothStep(startValue, newSpeed, t);
            yield return new WaitForFixedUpdate();
        }
        m_roadSpeed = newSpeed;
    }
}
