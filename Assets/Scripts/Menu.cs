using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Menu : MonoBehaviour
{
    private bool m_startAnimationOver = false;

    [SerializeField]
    private TextMeshProUGUI m_title;

    [SerializeField]
    private TextMeshProUGUI m_press;

    [SerializeField]
    private GameObject m_tutorialButton;

    [SerializeField]
    private TextMeshProUGUI m_tutorial;

    [SerializeField]
    private TextMeshProUGUI m_quitButton;

    private void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        m_press.text = "Press to Start";
#endif
        m_tutorialButton.SetActive(GameManager.Instance.TutoCompleted);
    }
    
    public void QuitGame()
    {
        // save any game data here
        #if UNITY_EDITOR
            // Application.Quit() does not work in the editor so
            // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    void Update()
    {
        if (m_startAnimationOver)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            if (Input.GetButtonDown("Submit"))
            {
                StartGame();
            }
#elif UNITY_ANDROID
            if (Input.touches.Length > 0)
            {
                StartGame();
            }
#endif
        }
    }

    private void StartGame()
    {
        GameManager.Instance.StartGame();
        gameObject.SetActive(false);
    }

    public void StartTutorial()
    {
        GameManager.Instance.TutoCompleted = false;
        StartGame();
    }

    public void StartAnimationOver()
    {
        gameObject.SetActive(true);
        StartCoroutine(DisplayText());
    }

    private IEnumerator DisplayText()
    {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime;
            m_title.color = new Color(m_title.color.r, m_title.color.g, m_title.color.b, t);
            m_press.color = new Color(m_title.color.r, m_title.color.g, m_title.color.b, t);
            m_tutorial.color = new Color(m_title.color.r, m_title.color.g, m_title.color.b, t);
            m_quitButton.color = new Color(m_title.color.r, m_title.color.g, m_title.color.b, t);
            yield return new WaitForFixedUpdate();
        }

        m_startAnimationOver = true;
    }
}
