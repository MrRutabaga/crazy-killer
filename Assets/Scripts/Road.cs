using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RoadType { EMPTY , OBSTACLES , CHECKPOINT , END_WALL };

public class Road : MonoBehaviour
{
    [SerializeField] private GameObject m_obstaclePrefab;
    [SerializeField] private GameObject m_endWallPrefab;
    [SerializeField] private GameObject m_checkPointPrefab;

    [SerializeField] private GameObject[] m_tutoObstaclePrefabs;

    [SerializeField]
    private Transform m_obstacleParent;

    private GameObject m_currentGraphicModule;
    private List<int> m_zObstacleValues = new List<int>();

    public Transform ObstacleParent => m_obstacleParent;
    public void GenerateRoads(RoadType type)
    {
        DeleteAll();

        GenerateGraphicModule();

        switch (type)
        {
            case RoadType.OBSTACLES:
                GenerateObstacles();
                break;
            
            case RoadType.CHECKPOINT:
                Instantiate(m_checkPointPrefab, m_obstacleParent);
                break;
            
            case RoadType.END_WALL:
                Instantiate(m_endWallPrefab, m_obstacleParent);
                break;
        }
    }

    private void GenerateGraphicModule()
    {
        int random = Random.Range(0, LevelManager.Instance.GraphicModules.Length);
        m_currentGraphicModule = Instantiate(LevelManager.Instance.GraphicModules[random], transform);
        m_currentGraphicModule.transform.localPosition = Vector3.zero;
    }

    private void GenerateObstacles()
    {
        if (!GameManager.Instance.TutoCompleted)
        {
            GameObject prefab = null;
            if (LevelManager.Instance.CurrentTutoStep < m_tutoObstaclePrefabs.Length)
                prefab = m_tutoObstaclePrefabs[LevelManager.Instance.CurrentTutoStep];
            else
                prefab = m_tutoObstaclePrefabs[Random.Range(0, m_tutoObstaclePrefabs.Length)];
            Instantiate(prefab, m_obstacleParent);
        }
        else
        {
            for (int i = 0; i < GameManager.Data.MaxObstaclesCountByChunk - 1; i++)
            {
                float currentZValue = LevelManager.Instance.RoadChunkLength * i /
                                      GameManager.Data.MaxObstaclesCountByChunk;
                if ((int) Random.Range(0f, 1f / GameManager.Data.ChancesOfObstacle) == 0)
                {
                    Transform obs = Instantiate(m_obstaclePrefab, m_obstacleParent).transform;
                    float obsSize = obs.GetComponent<BoxCollider>().size.x;
                    float xPos = Random.Range(-LevelManager.Instance.RoadWidth + obsSize / 2, LevelManager.Instance.RoadWidth - obsSize / 2);
                    obs.localPosition = new Vector3(xPos, 0.5f, -currentZValue);
                    m_zObstacleValues.Add((int) -currentZValue);
                    m_zObstacleValues.Add((int) -currentZValue + 1);
                    m_zObstacleValues.Add((int) -currentZValue - 1);
                }
            }
        }
    }

    private void DeleteAll()
    {
        // Modules
        if (m_currentGraphicModule)
            Destroy(m_currentGraphicModule);

        // Obstacles
        foreach (Transform child in m_obstacleParent)
        {
            Destroy(child.gameObject);
        }

        m_zObstacleValues.Clear();
    }
}
