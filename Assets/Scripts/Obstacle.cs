using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField]
    private Collider[] pieces;

    [SerializeField]
    private GameObject m_collisionFx;

    public void Explodes(Vector3 collisionPosition)
    {
        GetComponent<BoxCollider>().enabled = false;

        GameObject fx = Instantiate(m_collisionFx, collisionPosition, Quaternion.identity);
        fx.transform.parent = transform;
        Destroy(fx, 3f);

        foreach(Collider piece in pieces)
        {
            piece.enabled = true;
            Rigidbody rb = piece.GetComponent<Rigidbody>();
            rb.useGravity = true;
            rb.constraints = RigidbodyConstraints.None;
            rb.AddForce((piece.transform.position - collisionPosition + Vector3.forward) * GameManager.Data.ObstacleExplodeForce * LevelManager.Instance.RoadSpeed / 10f, ForceMode.Impulse);
        }

        Destroy(gameObject, 10f);
    }
}
