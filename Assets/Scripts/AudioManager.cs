﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region Singleton

    private static AudioManager m_instance;

    void Awake()
    {
        if (m_instance != null && m_instance != this)
            Destroy(gameObject);

        m_instance = this;
    }
    
    #endregion
    
    #region Serialized Fields
    
    [SerializeField] private AudioSource m_musicSource;
    [SerializeField] private AudioSource m_soundSource;
    [Space(10)]
    [SerializeField] private AudioClip m_gameMusic;
    [SerializeField] private AudioClip m_collisionSound;
    [SerializeField] private AudioClip m_homeMusic;

    #endregion
        
    #region Getters

    public static AudioSource MusicSource => m_instance.m_musicSource;
    public static AudioSource SoundSource => m_instance.m_soundSource;
    public static AudioClip GameMusic => m_instance.m_gameMusic;
    public static AudioClip CollisionSound => m_instance.m_collisionSound;
    public static AudioClip HomeMusic => m_instance.m_homeMusic;

    #endregion
}