using UnityEngine;

public class SlowMotionManager : MonoBehaviour
{
    private int m_currentStep = 0;
    
    public void NextStep()
    {
        if (m_currentStep++ == 0)
            Time.timeScale = Mathf.Min(1f, GameManager.Data.SlowMotionAtLowestSpeed * ((1 - LevelManager.Instance.RoadSpeed / GameManager.Data.MaxSpeed) * 1.4f));
        else
        {
            Time.timeScale = 1f;
            m_currentStep = 0;
        }
    }

    public void StartAnimation()
    {
        
    }
}
