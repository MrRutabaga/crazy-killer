using Unity.Mathematics;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Menu menuPanel;
    [SerializeField] private Animator m_anim;
    
    public void ChangedFlipData(ScreenOrientation orientation)
    {
        m_anim.SetBool("landscape", orientation == ScreenOrientation.LandscapeLeft || orientation == ScreenOrientation.LandscapeRight);
    }
    
    private void StartAnimationOver()
    {
        menuPanel.StartAnimationOver();
    }
}
