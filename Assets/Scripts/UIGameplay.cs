using System;
using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIGameplay : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_speed;
    [SerializeField]
    private TextMeshProUGUI m_timer;
    [SerializeField]
    private TextMeshProUGUI m_distance;

    [Space(10)]
    [SerializeField] private RawImage m_HTouch;
    [SerializeField] private RawImage m_HTouchReversed;
    [SerializeField] private RawImage m_VTouch;
    [SerializeField] private RawImage m_VTouchReversed;
    [SerializeField] private RawImage m_gyro;
    [SerializeField] private RawImage m_gyroReversed;

    void Update()
    {
        if (GameManager.Instance.IsStarted)
        {
            m_speed.text = "" + Mathf.Ceil(LevelManager.Instance.CarSpeed);
            m_timer.text = GameManager.Instance.TimerUpdate;
            m_distance.text = (LevelManager.Instance.DistanceLeft + 900).ToString("0000") + " m";
        }
    }
    
    public IEnumerator ShowControlUI()
    {
        RawImage UIToDisplay = null;

        if (GameManager.Instance.CurrentControl is Touch)
        {
            UIToDisplay = GameManager.Instance.CurrentControl is HTouchReversed ? m_HTouchReversed :
                GameManager.Instance.CurrentControl is VTouchReversed ? m_VTouchReversed :
                GameManager.Instance.CurrentControl is HTouch ? m_HTouch :
                m_VTouch;
        }
        else
            UIToDisplay = GameManager.Instance.CurrentControl is GyroReversed ? m_gyroReversed : m_gyro;

        float apparitionTime = .2f;
        float maxAlpha = .3f;
        float t = 0f;
        while (t / apparitionTime < 1f)
        {
            UIToDisplay.color = new Color(1f, 1f, 1f, t * maxAlpha);
            t += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        yield return new WaitForSeconds(1.5f);
        
        while (t >= 0f)
        {
            UIToDisplay.color = new Color(1f, 1f, 1f, t * maxAlpha);
            t -= Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        UIToDisplay.color = new Color(1f, 1f, 1f, 0f);
    }
}
