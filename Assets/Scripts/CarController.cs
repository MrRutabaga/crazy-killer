using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CarController : MonoBehaviour
{
    #region Singleton

    private static CarController m_instance;

    public static CarController Instance => m_instance;

    private void Awake()
    {
        if (m_instance != null && m_instance != this)
            Destroy(gameObject);
        m_instance = this;
    }
    
    #endregion
    
    [SerializeField] private Rigidbody m_rb;
    [SerializeField] private Animator m_anim;
    [SerializeField] private GameObject[] m_headlights;
    [SerializeField] private ParticleSystem[] m_drivingSmoke;
    [SerializeField] private GameObject m_fireFXPrefab;
    [SerializeField] private GameObject m_fireBurst;
    [SerializeField] private Transform m_fireBurstParent;
    [SerializeField] private RawImage m_speedUI;
    [SerializeField] private TextMeshProUGUI m_speedText;

    private GameObject m_fireFX;

    void FixedUpdate()
    {
        if (GameManager.Instance.IsStarted && !GameManager.Instance.IsOver)
        {
            m_speedText.text = (int)LevelManager.Instance.CarSpeed + " Km/h";
            if(GameManager.Instance.ClickInPause)
            {
                if (GameManager.Instance.CurrentControl is Gyro || !(GameManager.Instance.CurrentControl as Touch).isTouching())
                    GameManager.Instance.ClickInPause = false;
            }
            
            if(!GameManager.Instance.ClickInPause)
            {
                float direction = 0f;
                direction = GameManager.Instance.CurrentControl.getDirection();
                m_rb.AddForce(Vector3.right * direction * GameManager.CurrentControlData.XSpeed *
                            GameManager.CurrentControlData.XSpeedCurve
                                .Evaluate(LevelManager.Instance.CarSpeed / GameManager.Data.MaxSpeed),
                    ForceMode.Acceleration);
            }
            m_rb.velocity -= Vector3.right * m_rb.velocity.x * GameManager.CurrentControlData.XFriction;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
        {
            GameManager.Instance.HitObstacle(col);
            StartCoroutine(HeadlightBlink(!GameManager.Instance.IsOver));
        }
        else if (col.gameObject.layer == LayerMask.NameToLayer("Props"))
        {
            GameManager.Instance.HitProps(col);
        }
        else if (col.gameObject.layer == LayerMask.NameToLayer("EndWall"))
        {
            if (!GameManager.Instance.IsOver)
                GameManager.Instance.End(true, true);
        }
    }

    public void Initialize(bool headlightsOn)
    {
        InFire(false);
        foreach (ParticleSystem smoke in m_drivingSmoke)
            smoke.Play();

        StartCoroutine(HeadlightBlink(headlightsOn));
    }

    public void Stop()
    {
        foreach (GameObject light in m_headlights)
            light.SetActive(false);

        StartCoroutine(DisplayUI(false));
        m_rb.velocity = Vector3.zero;
    }

    public IEnumerator DisplayUI(bool toggle = true)
    {
        if (toggle == m_speedUI.IsActive())
            yield break;
        
        if (toggle)
            m_speedUI.gameObject.SetActive(true);
        
        float displayTime = .4f;
        float maxAlpha = .6f;
        float t = 0f;
        while (t < 1f)
        {
            m_speedUI.color = new Color(1f, 1f, 1f, (toggle ? t : 1 - t) * maxAlpha);
            m_speedText.color = new Color(1f, 1f, 1f, (toggle ? t : 1 - t) * maxAlpha);
            t += Time.deltaTime / displayTime;
            yield return new WaitForFixedUpdate();
        }
        
        if (!toggle)
            m_speedUI.gameObject.SetActive(false);
    }
    
    private IEnumerator HeadlightBlink(bool on)
    {
        int blinkCount = Random.Range(2, 4);
        for (int i = 0; i < blinkCount; i++)
        {
            SetHeadlights(!on);
            yield return new WaitForSeconds(Random.Range(.05f, .15f));
            SetHeadlights(on);
            yield return new WaitForSeconds(Random.Range(.03f, .12f));
        }
        
        void SetHeadlights(bool on)
        {
            foreach (GameObject light in m_headlights)
                light.SetActive(on);
        }
    }

    public void StopParticles()
    {
        foreach (ParticleSystem smoke in m_drivingSmoke)
            smoke.Stop();
    }

    public IEnumerator Center()
    {
        m_rb.velocity = Vector2.zero;
        Vector3 oldPos = transform.position;
        Vector3 newPos = oldPos;
        newPos.x = 0f;
        float t = 0f;
        float moveTime = .6f;
        while (t < 1f * moveTime)
        {
            t += Time.deltaTime;
            transform.position = Vector3.Slerp(oldPos, newPos, t / moveTime);
            yield return new WaitForFixedUpdate();
        }
    }

    public void Rotate(Control newControl)
    {
        m_anim.SetTrigger(Control.GetName(newControl));
        
        // Old code
        /*Vector3 oldRot = transform.rotation.eulerAngles;
        Vector3 newRot = oldRot;
        newRot.y += 180f;
        float t = 0f;
        float rotationTime = .6f;
        while (t < 1f * rotationTime)
        {
            t += Time.deltaTime;
            transform.rotation = Quaternion.Slerp(Quaternion.Euler(oldRot), Quaternion.Euler(newRot), t / rotationTime);
            yield return new WaitForFixedUpdate();
        }*/
    }

    public void InFire(bool on)
    {
        if (m_fireFX)
            Destroy(m_fireFX);
        if (on)
            m_fireFX = Instantiate(m_fireFXPrefab, transform);
    }
}
          