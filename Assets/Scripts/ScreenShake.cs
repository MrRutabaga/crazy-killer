using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    private Transform m_shakeContainer;
    private float shakeTimeRemaining, shakePower, shakeFadeTime;
    private Vector3 oldPosition;
    private bool isShaking = false;

    private void Start()
    {
        m_shakeContainer = Instantiate(new GameObject("ShakeContainer")).transform;
        transform.SetParent(m_shakeContainer);
        oldPosition = m_shakeContainer.transform.position;
    }

    public void StartShake(float lenght, float power){
        shakeTimeRemaining = lenght;
        shakePower = power;

        shakeFadeTime = power / lenght;
        isShaking = true;
    }

    private void LateUpdate(){
        if(shakeTimeRemaining > 0)
        {
            shakeTimeRemaining -= Time.deltaTime;
            float xAmount = Random.Range(-1f,1f) * shakePower;           
            float yAmount = Random.Range(-1f,1f) * shakePower;

            m_shakeContainer.transform.position = oldPosition + new Vector3(xAmount, yAmount, 0);

            shakePower = Mathf.MoveTowards(shakePower, 0f, shakeFadeTime * Time.deltaTime);
        }
        else if (isShaking)
        {
            m_shakeContainer.transform.position = oldPosition;
            isShaking = false;
        }
    }
}
