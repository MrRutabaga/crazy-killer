using System.Collections.Generic;
using UnityEngine;

public abstract class Control
{
    public static List<Control> Controls = new List<Control> {
        new HTouch(), // First is default controls (start of the game)
        new HTouchReversed(),
        new VTouch(),
        new VTouchReversed(),
#if UNITY_ANDROID && !UNITY_EDITOR
        new Gyro(),
        new GyroReversed()
#endif
    };

    public static string GetName(Control c)
    {
        if (c is HTouch)
            return c is HTouchReversed ? "HTouchReversed" : "HTouch";
        if (c is VTouch)
            return c is VTouchReversed ? "VTouchReversed" : "VTouch";
        return c is GyroReversed ? "HTouchReversed" : "HTouch";
    }

    public abstract float getDirection();
}

public abstract class Touch : Control
{
    public bool isTouching()
    {
#if UNITY_EDITOR
        return Input.GetMouseButton(0);
#elif UNITY_ANDROID
        return Input.touches.Length > 0;
#endif
    }
    
    protected Vector2 getTouchPosition()
    {
#if UNITY_EDITOR
        return Input.mousePosition;
#elif UNITY_ANDROID
        return Input.touches[0].position;
#endif
    }
}

public class HTouch : Touch
{
    public override float getDirection()
    {
        if (!isTouching())
            return 0f;

        return getTouchPosition().x > Screen.width / 2 ?
            1f : -1f;
    }
}

public class HTouchReversed : HTouch
{
    public override float getDirection()
    {
        if (!isTouching())
            return 0f;

        return getTouchPosition().x < Screen.width / 2 ?
            1f : -1f;
    }
}

public class VTouch : Touch
{
    public override float getDirection()
    {
        if (!isTouching())
            return 0f;

        return getTouchPosition().y > Screen.height / 2 ?
            -1f : 1f;
    }
}

public class VTouchReversed : VTouch 
{
    public override float getDirection()
    {
        if (!isTouching())
            return 0f;

        return getTouchPosition().y < Screen.height / 2 ?
            -1f : 1f;
    }
}

public class Gyro : Control
{
    public override float getDirection()
    {
        float orient = -(Input.gyro.attitude.eulerAngles.z - 270f) / ((GyroData)GameManager.CurrentControlData).MaxAngle;
        if (orient < ((GyroData)GameManager.CurrentControlData).DeadZone && orient > -((GyroData)GameManager.CurrentControlData).DeadZone)
            return 0;
        
        return Mathf.Max(-1, Mathf.Min(1, orient));
    }
}

public class GyroReversed : Gyro
{
    public override float getDirection()
    {
        float orient = (Input.gyro.attitude.eulerAngles.z - 270f) / ((GyroData)GameManager.CurrentControlData).MaxAngle;
        if (orient < ((GyroData)GameManager.CurrentControlData).DeadZone && orient > -((GyroData)GameManager.CurrentControlData).DeadZone)
            return 0;
        
        return Mathf.Max(-1, Mathf.Min(1, orient));
    }
}
