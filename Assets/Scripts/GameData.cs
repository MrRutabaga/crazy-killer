using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="GameData", menuName="Game Data")]
public class GameData : ScriptableObject
{
        
    #region Serialized Fields
        
    [Header("Game")]
    [SerializeField] private float m_levelDistance = 1000;
    [SerializeField] private float m_minSpeedToLose = 0f;
    [SerializeField] private ControlsData m_controlsData;

    [Header("Level")]
    [SerializeField] private int m_maxObstaclesCountByChunk = 6;
    [SerializeField] private float m_minXSpaceBetweenObstacle = 2f; // x then z
    [SerializeField, Range(0f, 1f)] private float m_chancesOfObstacle = 0.65f;
    [SerializeField] private float m_distanceBetweenCheckpoints = 300f;

    [Header("Car")]
    [SerializeField] private float m_startYSpeed = 15f; // m/s
    [SerializeField] private AnimationCurve m_accelerationCurve;
    [SerializeField] private float m_maxSpeed = 56f;
    [SerializeField] private float m_speedChangeLerpSpeed = 3f;

    [Header("Obstacles")]
    [SerializeField] private float m_obstacleForce = 6f;
    [SerializeField] private float m_obstacleExplodeForce = 7f;
    [SerializeField] private float m_obstacleShakeTime = .4f;
    [SerializeField] private float m_obstacleShakePower = .8f;
    
    [Header("Checkpoints")]
    [SerializeField, Range(0f, 1f)] private float m_checkpointFlipChances = 0.3f;

    [SerializeField, Range(0f, 1f)] private float m_slowMotionAtLowestSpeed = 1f;


    #endregion
        
    #region Getters

    public ControlsData ControlsData => m_controlsData;
    public float LevelDistance => m_levelDistance;
    public float MinSpeedToLose => m_minSpeedToLose;
    public int MaxObstaclesCountByChunk => m_maxObstaclesCountByChunk;
    public float MinXSpaceBetweenObstacle => m_minXSpaceBetweenObstacle;
    public float ChancesOfObstacle => m_chancesOfObstacle;
    public float DistanceBetweenCheckpoints => m_distanceBetweenCheckpoints;
    public float StartYSpeed => m_startYSpeed;
    public AnimationCurve AccelerationCurve => m_accelerationCurve;
    public float MaxSpeed => m_maxSpeed;
    public float SpeedChangeLerpSpeed => m_speedChangeLerpSpeed;
    public float ObstacleForce => m_obstacleForce;
    public float ObstacleExplodeForce => m_obstacleExplodeForce;
    public float ObstacleShakeTime => m_obstacleShakeTime;
    public float ObstacleShakePower => m_obstacleShakePower;
    public float CheckpointFlipChances => m_checkpointFlipChances;
    public float SlowMotionAtLowestSpeed => m_slowMotionAtLowestSpeed;

    #endregion

}

[Serializable]
public class ControlsData
{
    [SerializeField] private TouchData m_touch;
    [SerializeField] private GyroData m_gyro;

    public ControlData getControlData(Control currentControl)
    {
        if (currentControl is Touch)
            return m_touch;
        return m_gyro;
    }
}

[Serializable]
public class ControlData
{
    [SerializeField] private float m_xSpeed;
    [SerializeField] private AnimationCurve m_xSpeedCurve;
    [SerializeField] private float m_xFriction;

    public float XSpeed => m_xSpeed;
    public AnimationCurve XSpeedCurve => m_xSpeedCurve;
    public float XFriction => m_xFriction;
}

[Serializable]
public class TouchData : ControlData
{
    
}

[Serializable]
public class GyroData : ControlData
{
    [SerializeField, Range(0f, 1f)] private float m_deadZone = 0.1f;
    [SerializeField, Range(0, 360)] private int m_maxAngle = 30;
    
    public float DeadZone => m_deadZone;
    public float MaxAngle => m_maxAngle;
}