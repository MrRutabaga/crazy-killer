using System.Collections;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.SceneManagement;
using RenderSettings = UnityEngine.RenderSettings;

public class GameManager : MonoBehaviour
{
    
    #region Singleton

    private static GameManager m_instance;
    private static GameData m_data;

    public static GameManager Instance => m_instance;
    public static GameData Data => m_data;
    public static ControlData CurrentControlData;
    
    private void Awake()
    {
        if (m_instance != null && m_instance != this)
            Destroy(gameObject);

        m_instance = this;
        
        m_data = m_gameData;
        m_mainCameraController = m_mainCamera.GetComponent<CameraController>();
    }

    #endregion

    #region Private Attributes

    private LevelManager m_levelManager = null;
    private Control m_currentControl;
    private ScreenOrientation m_currentOrientation;
    private CameraController m_mainCameraController;

    private bool m_isStarted = false;
    private bool m_isOver = false;
    private bool m_isPaused = false;

    private string m_timer = "00:00";
    private string m_timerUpdate = "00:00";
    private float m_startTime;
    
    #endregion

    #region Serialized Fields

    [SerializeField] private GameData m_gameData;
    [SerializeField] private CarController m_car = null;
    [SerializeField] private Camera m_mainCamera;
    [SerializeField] private ScreenShake m_screenShake;
    [SerializeField] private Restart m_restartPanel;
    [SerializeField] private UIGameplay m_uiPanel;
    [SerializeField] private GameObject m_ingameDataPanel;
    [SerializeField] private GameObject m_menu;
    [SerializeField] private Options m_options;
    [SerializeField] private Light m_light;
    [SerializeField] private Material m_daySkybox;
    [SerializeField] private Material m_nightSkybox;
    [SerializeField] private GameObject m_lanternPrefab;

    #endregion

    #region Getters

    public Control CurrentControl
    {
        get => m_currentControl;
        set
        {
            if (CurrentControl == value)
                return;
            
            m_car.Rotate(value);
            m_currentControl = value;
            CurrentControlData = Data.ControlsData.getControlData(value);
        }
    }
    public ScreenOrientation CurrentOrientation
    {
        get => m_currentOrientation;
        set
        {
            m_currentOrientation = value;
            Screen.orientation = value;
            m_mainCameraController.ChangedFlipData(value);
        }
    }

    public bool TutoCompleted = false;
    public bool ClickInPause = false;
    public bool ToggleHelp = true;
    public bool ToggleJour = true;
    public bool IsStarted => m_isStarted;
    public bool IsOver => m_isOver;
    public string Timer => m_timer;
    public string TimerUpdate => m_timerUpdate;

    #endregion

    #region Load

    private void Start()
    {
#if UNITY_ANDROID
        Input.gyro.enabled = true;
#endif

        if (PlayerPrefs.HasKey("TutoCompleted"))
            TutoCompleted = PlayerPrefs.GetInt("TutoCompleted") == 1;
        m_options.GetBackSavedOptions();
        
        StartCoroutine(LoadLevel());
    }

    private IEnumerator LoadLevel()
    {
        // Unload old level
        if (SceneManager.sceneCount > 1)
        {
            var oldLevelUnloaded = SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Level"));
            yield return oldLevelUnloaded;
        }
        
        // Load new level
        var levelLoaded = SceneManager.LoadSceneAsync("Level", LoadSceneMode.Additive);
        yield return levelLoaded;
        m_levelManager = FindObjectOfType<LevelManager>();

        // Destroy all other cameras
        foreach(Camera cam in FindObjectsOfType<Camera>())
        {
            if (cam != m_mainCamera)
                Destroy(cam.gameObject);
        }

        m_car.Initialize(false);
        CurrentControl = Control.Controls[Random.Range(0, Control.Controls.Count)];
    }
    
    #endregion

    #region Game

    public void StartGame()
    {
        m_levelManager.Initialize();
        CurrentControl = Control.Controls[0];
        CurrentOrientation = ScreenOrientation.Portrait;
        
        // Music
        AudioManager.MusicSource.Stop();
        AudioManager.MusicSource.clip = AudioManager.GameMusic;
        AudioManager.MusicSource.Play();
        
        // Bools
        m_isStarted = true;
        m_isOver = false;
        
        // UI
        m_uiPanel.gameObject.SetActive(true);
        if (TutoCompleted)
        {
            m_startTime = Time.time;
            m_timer = "";
            
            StartCoroutine(m_car.DisplayUI());
            m_ingameDataPanel.SetActive(true);
        }
        else
        {
            m_ingameDataPanel.SetActive(false);
            ShowControlUI();
        }

        // Car
        m_car.Initialize(true);
    }
    
    void Update()
    {
        if(m_isStarted && !m_isPaused && Input.GetButtonDown("Cancel"))
            Pause();
        
        float t = Time.time - m_startTime;
        string minutes = ((int) t/60).ToString("00");
        string seconds = ((int)(t%60)).ToString("00");
        m_timerUpdate = minutes + ":" + seconds;
    }

    public void PauseInput()
    {
        if (m_isPaused)
            GameManager.Instance.ClickInPause = true;
        Pause();
    }

    private void Pause()
    {
        Time.timeScale = m_isPaused ? 1 : 0;
        m_isPaused = !m_isPaused;
        m_options.gameObject.SetActive(m_isPaused);
    }

    public void OpenOptions()
    {
        m_menu.SetActive(false);
        m_options.gameObject.SetActive(true);
    }

    private void StopGame()
    {
        AudioManager.MusicSource.Stop();
        AudioManager.MusicSource.clip = AudioManager.HomeMusic;
        AudioManager.MusicSource.Play();
        
        if (TutoCompleted)
            StartCoroutine(m_car.DisplayUI(false));
        
        m_car.Stop();
        m_car.InFire(false);
    }

    public void BackMenu()
    {
        if (m_isStarted)
        {
            StopGame();
            m_uiPanel.gameObject.SetActive(false);
            if(m_isPaused)
                Pause();
            m_isStarted = false;
            StartCoroutine(m_levelManager.LerpSpeed(Data.StartYSpeed));
            m_levelManager.ClearRoad();
            if (PlayerPrefs.HasKey("TutoCompleted"))
                TutoCompleted = true;
            CurrentOrientation = ScreenOrientation.Portrait;
            StartCoroutine(m_car.Center());
        }

        m_options.gameObject.SetActive(false);
        m_menu.SetActive(true);
    }

    public void End(bool win, bool instant = false)
    {
        if (win)
            m_screenShake.StartShake(.5f, 1f);
        if (!instant)
            StartCoroutine(m_levelManager.LerpSpeed(0f));
        m_timer = m_timerUpdate;
        m_isOver = true;
        m_uiPanel.gameObject.SetActive(false);
        m_restartPanel.gameObject.SetActive(true);
        m_restartPanel.UpdateText();
        m_car.Stop();
        m_car.StopParticles();
        m_car.InFire(true);
    }

    public void RestartGame()
    {
        StartCoroutine(LoadLevel());
        StartGame();
    }
    
    #endregion

    #region Speed

    private void Decelerate(float speedChange)
    {
        if ((m_levelManager.RoadSpeed -= speedChange) <= Data.MinSpeedToLose)
        {
            if (m_levelManager.RoadSpeed <= 0f)
                m_levelManager.RoadSpeed = 0f;
            
            End(false);
        }
    }

    public void HitObstacle(Collision col)
    {
        m_screenShake.StartShake(Data.ObstacleShakeTime, Data.ObstacleShakePower);
        Decelerate(Data.ObstacleForce);
        col.gameObject.GetComponent<Obstacle>().Explodes(m_car.transform.position);
        
        AudioManager.SoundSource.clip = AudioManager.CollisionSound;
        AudioManager.SoundSource.Play();
    }

    public void HitProps(Collision col)
    {
        m_screenShake.StartShake(Data.ObstacleShakeTime, Data.ObstacleShakePower);
        col.gameObject.GetComponent<Rigidbody>().AddForce((transform.position - m_car.transform.position + Vector3.forward) * GameManager.Data.ObstacleExplodeForce * LevelManager.Instance.RoadSpeed / 10f, ForceMode.Impulse);
    }

    #endregion

    public void ShowControlUI()
    {
        if (ToggleHelp || !TutoCompleted)
            StartCoroutine(m_uiPanel.ShowControlUI());
    }

    public void CompleteTuto()
    {
        PlayerPrefs.SetInt("TutoCompleted", 1);
        m_ingameDataPanel.gameObject.SetActive(true);
        StartCoroutine(m_car.DisplayUI());
        m_startTime = Time.time;
        m_timer = "";
    }

    public void JourNuit(bool jour)
    {
        ToggleJour = jour;
        if (jour)
        {
            RenderSettings.fogColor = Color.white;
            RenderSettings.fogStartDistance = 25;
            RenderSettings.fogEndDistance = 300;
            RenderSettings.skybox = m_daySkybox;
            RenderSettings.ambientIntensity = 1f;
            m_light.intensity = 1f;
        }
        else
        {
            RenderSettings.fogColor = Color.black;
            RenderSettings.fogStartDistance = 10;
            RenderSettings.fogEndDistance = 75;
            RenderSettings.skybox = m_nightSkybox;
            RenderSettings.ambientIntensity = 0f;
            m_light.intensity = .3f;
        }

        foreach (GameObject lantern in GameObject.FindGameObjectsWithTag("lantern"))
        {
            lantern.GetComponentInChildren<Light>().enabled = !jour;
        }
    }
}
