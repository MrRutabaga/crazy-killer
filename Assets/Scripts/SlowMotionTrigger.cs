using UnityEngine;

public class SlowMotionTrigger : MonoBehaviour
{
    [SerializeField] private SlowMotionManager m_slowMotionManager;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            m_slowMotionManager.NextStep();
        }
    }
}
