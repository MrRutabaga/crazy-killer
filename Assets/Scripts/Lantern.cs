using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lantern : MonoBehaviour
{
    [SerializeField]
    private Light m_light;
    
    void Start()
    {
        m_light.enabled = !GameManager.Instance.ToggleJour;
    }
}
