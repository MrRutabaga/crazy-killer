using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    [SerializeField] private Toggle m_music;
    [SerializeField] private Toggle m_son;
    [SerializeField] private Toggle m_jourNuit;
    [SerializeField] private Toggle m_help;
    [SerializeField] private Toggle m_gyro;
    
    public void GetBackSavedOptions()
    {
        if (PlayerPrefs.HasKey("options.togglemusic"))
            m_music.isOn = PlayerPrefs.GetInt("options.togglemusic") == 1;
        else m_music.isOn = true;

        if (PlayerPrefs.HasKey("options.toggleson"))
            m_son.isOn = PlayerPrefs.GetInt("options.toggleson") == 1;
        else m_son.isOn = true;

        if (PlayerPrefs.HasKey("options.togglejour"))
            m_jourNuit.isOn = PlayerPrefs.GetInt("options.togglejour") == 1;
        else m_jourNuit.isOn = true;

        if (PlayerPrefs.HasKey("options.togglehelp"))
            m_help.isOn = PlayerPrefs.GetInt("options.togglehelp") == 1;
        else m_help.isOn = true;

#if UNITY_ANDROID && !UNITY_EDITOR
        if (PlayerPrefs.HasKey("options.togglegyro"))
            m_gyro.isOn = PlayerPrefs.GetInt("options.togglegyro") == 1;
        else m_gyro.isOn = true;
#else
        m_gyro.gameObject.SetActive(false);
#endif
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (GameManager.Instance.IsStarted)
                GameManager.Instance.PauseInput();
            else
                BackMenu();
        }
    }

    public void BackMenu()
    {
        GameManager.Instance.BackMenu();
    }

    public void ToggleMusic()
    {
        AudioManager.MusicSource.mute = !m_music.isOn;
        PlayerPrefs.SetInt("options.togglemusic", m_music.isOn ? 1 : 0);
    }

    public void ToggleSon()
    {
        AudioManager.SoundSource.mute = !m_son.isOn;
        PlayerPrefs.SetInt("options.toggleson", m_son.isOn ? 1 : 0);
    }

    public void ToggleJour()
    {
        PlayerPrefs.SetInt("options.togglejour", m_jourNuit.isOn ? 1 : 0);
        GameManager.Instance.JourNuit(m_jourNuit.isOn);
    }

    public void ToggleHelp()
    {
        PlayerPrefs.SetInt("options.togglehelp", m_help.isOn ? 1 : 0);
        GameManager.Instance.ToggleHelp = m_help.isOn;
    }

    public void ToggleGyro()
    {
        PlayerPrefs.SetInt("options.togglegyro", m_gyro.isOn ? 1 : 0);
        if (!m_gyro.isOn)
            Control.Controls.RemoveRange(Control.Controls.Count - 2, 2);
        else
        {
            Control.Controls.Add(new Gyro());
            Control.Controls.Add(new GyroReversed());
        }
    }
}
