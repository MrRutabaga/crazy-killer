using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    [SerializeField] private SlowMotionManager m_slowmoManager;
    
    [Space(10)]
    [SerializeField] private SpriteRenderer m_bigSignIcon;
    [SerializeField] private SpriteRenderer m_bigSign;
    [SerializeField] private SpriteRenderer m_rightSign;
    [SerializeField] private SpriteRenderer m_leftSign;

    [Header("Icons")] [SerializeField] private Sprite m_portraitTouchHSprite;
    [SerializeField] private Sprite m_portraitTouchHSpriteReversed;
    [SerializeField] private Sprite m_portraitTouchVSprite;
    [SerializeField] private Sprite m_portraitTouchVSpriteReversed;
    [SerializeField] private Sprite m_landscapeTouchHSprite;
    [SerializeField] private Sprite m_landscapeTouchHSpriteReversed;
    [SerializeField] private Sprite m_landscapeTouchVSprite;
    [SerializeField] private Sprite m_landscapeTouchVSpriteReversed;
    [SerializeField] private Sprite m_gyroSprite;
    [SerializeField] private Sprite m_gyroSpriteReversed;
    [SerializeField] private Sprite m_flipPortrait;
    [SerializeField] private Sprite m_flipPortraitReversed;
    [SerializeField] private Sprite m_flipLandscape;
    [SerializeField] private Sprite m_flipLandscapeReversed;

    [Header("Signs")] [SerializeField] private Sprite m_redLineSprite;
    [SerializeField] private Sprite m_blueLineSprite;
    [SerializeField] private Sprite m_whiteLineSprite;
    [SerializeField] private Sprite m_redSprite;
    [SerializeField] private Sprite m_blueSprite;

    private Control m_nextControl;
    private ScreenOrientation m_nextOrientation;

    private ScreenOrientation[] m_orients =
    {
        ScreenOrientation.Portrait, ScreenOrientation.LandscapeRight, ScreenOrientation.PortraitUpsideDown,
        ScreenOrientation.LandscapeLeft
    };

    private bool m_tutoEnd = false;

    void Start()
    {
        Sprite newIcon = null;
        Sprite newBigSign = m_whiteLineSprite;
        Sprite newLeftSign = null;
        Sprite newRightSign = null;

        bool tutoOrient = false;
        bool tutoFlip = false;

        if (!GameManager.Instance.TutoCompleted)
        {
            int currentTutoStep = LevelManager.Instance.CurrentTutoStep++;
            if (currentTutoStep < 3)
            {
                tutoOrient = true;
                m_nextControl = Control.Controls[currentTutoStep + 1];
            }
#if UNITY_ANDROID && !UNITY_EDITOR
            else if (currentTutoStep == 3)
                m_nextOrientation = ScreenOrientation.LandscapeRight;
#endif
            else if (currentTutoStep < Control.Controls.Count - 1)
            {
                tutoOrient = true;
                m_nextControl = Control.Controls[currentTutoStep + 1];
            }
            else
            {
                tutoOrient = true;
                m_nextControl = Control.Controls[0];
                m_tutoEnd = true;
                GameManager.Instance.TutoCompleted = true;
            }

            tutoFlip = !tutoOrient;
        }

#if UNITY_ANDROID //&& !UNITY_EDITOR
        if (!tutoOrient && (tutoFlip || Random.Range(0f, 1f) <= GameManager.Data.CheckpointFlipChances))
        {
            m_nextControl = null;
            
            if (GameManager.Instance.TutoCompleted)
            {
                do
                {
                    m_nextOrientation = m_orients[Random.Range(0, 4)];
                } while (m_nextOrientation == GameManager.Instance.CurrentOrientation);
            }
            
            newIcon = m_nextOrientation == ScreenOrientation.Portrait ? m_flipPortrait :
                m_nextOrientation == ScreenOrientation.PortraitUpsideDown ? m_flipPortraitReversed :
                m_nextOrientation == ScreenOrientation.LandscapeRight ? m_flipLandscape : m_flipLandscapeReversed;
        }
        else
#endif
        {
            if (GameManager.Instance.TutoCompleted)
            {
                do
                {
                    m_nextControl = Control.Controls[Random.Range(0, Control.Controls.Count)];
                } while (m_nextControl == GameManager.Instance.CurrentControl);
            }

            if (m_nextControl is Touch)
            {
                if (m_nextControl is HTouch)
                {
                    if (GameManager.Instance.CurrentOrientation == ScreenOrientation.Portrait)
                        newIcon = m_nextControl is HTouchReversed
                            ? m_portraitTouchHSpriteReversed
                            : m_portraitTouchHSprite;
                    else
                        newIcon = m_nextControl is HTouchReversed
                            ? m_landscapeTouchHSpriteReversed
                            : m_landscapeTouchHSprite;

                    newLeftSign = m_nextControl is HTouchReversed ? m_blueSprite : m_redSprite;
                    newRightSign = m_nextControl is HTouchReversed ? m_redSprite : m_blueSprite;
                }
                else
                {
                    if (GameManager.Instance.CurrentOrientation == ScreenOrientation.Portrait)
                        newIcon = m_nextControl is VTouchReversed
                            ? m_portraitTouchVSpriteReversed
                            : m_portraitTouchVSprite;
                    else
                        newIcon = m_nextControl is VTouchReversed
                            ? m_landscapeTouchVSpriteReversed
                            : m_landscapeTouchVSprite;

                    newBigSign = m_nextControl is VTouchReversed ? m_blueLineSprite : m_redLineSprite;
                    newLeftSign = m_nextControl is VTouchReversed ? m_redSprite : m_blueSprite;
                    newRightSign = newLeftSign;
                }
            }
            else if (m_nextControl is Gyro)
            {
                newIcon = m_nextControl is GyroReversed ? m_gyroSpriteReversed : m_gyroSprite;
                newLeftSign = m_nextControl is GyroReversed ? m_blueSprite : m_redSprite;
                newRightSign = m_nextControl is GyroReversed ? m_redSprite : m_blueSprite;
            }
        }

        m_bigSignIcon.sprite = newIcon;
        m_bigSign.sprite = newBigSign;
        m_rightSign.sprite = newRightSign;
        m_leftSign.sprite = newLeftSign;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (m_nextControl != null)
            {
                GameManager.Instance.CurrentControl = m_nextControl;
                GameManager.Instance.ShowControlUI();
            }
            else
                GameManager.Instance.CurrentOrientation = m_nextOrientation;

            Time.timeScale = 1f;

            if (m_tutoEnd)
                GameManager.Instance.CompleteTuto();

            m_slowmoManager.StartAnimation();
        }
    }
}
