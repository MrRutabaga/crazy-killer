using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Restart : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_title;

    public TextMeshProUGUI m_timer;
    public TextMeshProUGUI m_distance;

#if UNITY_ANDROID
    private void Start()
    {
        m_title.text = "Press to Restart";
    }
#endif
    
    public void UpdateText()
    {
        m_timer.text = GameManager.Instance.Timer;
        m_distance.text = (int)LevelManager.Instance.DistanceMade + " m";
    }

    void Update()
    {
#if UNITY_STANDALONE
        if(Input.GetButtonDown("Submit"))
        {
            RestartGame();
        }
#elif UNITY_ANDROID
    #if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            RestartGame();
        }
    #else
        foreach(UnityEngine.Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                RestartGame();
            }
        }
    #endif
#endif
    }

    private void RestartGame()
    {
        gameObject.SetActive(false);
        GameManager.Instance.RestartGame();
    }
}
